package basetest;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class BaseTest {
	protected final Logger logTest = Logger.getLogger(getClass());
	protected WebDriver driver;
	public static final String environment = "test";
	public static final String baseurl = ReadProperties.getProperty("baseurl");

	protected static final String email = ReadProperties.getProperty("email");
	protected static final String password = ReadProperties.getProperty("password");

	@Before
	public void setUpDriver() {
		PropertyConfigurator.configure("src/test/resources/conf/log4j.properties");
		logTest.info(baseurl + " " + email);
		driver=new SetupDriver().createLocalDriver();
		driver.manage().window().fullscreen();
		driver.get(baseurl);
	}

	@Test
	public void deneme() {

	}

	@After
	public void tearDown() {
		driver.quit();
	}

}