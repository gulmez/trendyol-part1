package constants;

import org.openqa.selenium.By;

public final class LoginPageConstants {
	public static final By ACCOUNT_USER = By.className("account-user");
	public static final By LOGIN_BUTTON = By.className("login-button");
	public static final By EMAIL_INPUT = By.cssSelector("[data-testid='email-input']");
	public static final By PASSWORD_INPUT = By.cssSelector("[data-testid='password-input']");
	public static final By SUBMIT = By.className("submit");
	public static final String USER_NAME = ".user-name";
	public static final By I_USER = By.className("i-user");
	public static final By FANCYBOX_CLOSE = By.className("fancybox-close");
	public static final By MODAL_CLOSE = By.className("modal-close");
}
