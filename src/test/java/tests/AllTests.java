package tests;

import org.junit.Test;

import basetest.BaseTest;
import pages.LoginPage;

public class AllTests extends BaseTest {

	@Test
	public void trendyol_part1() {
		new LoginPage(driver).login(email, password).checkMainPageTabs().clickAndCheckRandomComponent()
				.checkAndAddToBasket();
	}
}
