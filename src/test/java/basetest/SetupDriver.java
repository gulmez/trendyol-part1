package basetest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class SetupDriver {
	protected WebDriver driver;
	protected String driverName = ReadProperties.getProperty("drivername");
	private DesiredCapabilities capabilities;

	public WebDriver createLocalDriver() {
		return setDriverConf(driverName);
	}

	@SuppressWarnings("deprecation")
	public WebDriver setDriverConf(String drivername) {
		switch (drivername) {
		case "chrome":
			capabilities = DesiredCapabilities.chrome();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-fullscreen");
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			System.setProperty("webdriver.chrome.driver", "path/chromedriver.exe");
			driver = new ChromeDriver(capabilities);
			break;
		default:
			break;

		case "firefox":
			capabilities = DesiredCapabilities.firefox();
			System.setProperty("webdriver.gecko.driver", "path/geckodriver.exe");
			driver = new FirefoxDriver(capabilities);
			break;

		}
		return driver;
	}
}
