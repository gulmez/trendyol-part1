package base;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	protected final Logger log = Logger.getLogger(getClass());
	protected WebDriver driver;
	static JavascriptExecutor jsDriver = null;

	public BasePage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement sendKeysBy(By by, String value) {

		WebElement element = getElementBy(by);
		element.clear();
		element.sendKeys(value);
		return element;
	}

	public void clickByWebElement(WebElement el) {
		scrollToElement(el);
		el.click();
	}

	public String elementGetText(String cssSelector) {
		pageLoadComplete();
		jsDriver = (JavascriptExecutor) driver;

		return jsDriver.executeScript("return document.querySelectorAll('" + cssSelector + "')[0].innerText", true)
				.toString();
	}

	public WebElement clickObjectBy(By by) {
		WebElement element = getElementBy(by);
		scrollToElement(element);
		element.click();
		return element;
	}

	public WebElement getElementBy(By by) {
		pageLoadComplete();
		return driver.findElement(by);
	}

	public List<WebElement> getElementsBy(By by) {
		pageLoadComplete();
		return driver.findElements(by);
	}

	public void pageLoadComplete() {
		jsDriver = (JavascriptExecutor) driver;
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {

			public Boolean apply(WebDriver driver) {
				return jsDriver.executeScript("return document.readyState", true).toString().equals("complete");
			}
		};
		try {
			sleep(1000);
			WebDriverWait wait = new WebDriverWait(driver, 200);
			wait.until(expectation);
		} catch (Throwable error) {
		}
	}

	public void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			log.debug(e.getMessage(), e);
		}
	}

	public boolean isElementPresent(By by) {
		try {
			getElementBy(by);
			return true;
		} catch (NoSuchElementException e) {
			log.debug(e.getMessage(), e);
			return false;
		}
	}

	public boolean isElementPresent(WebElement element, By by) {
		try {

			element.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			log.debug(e.getMessage(), e);
			return false;
		}
	}

	public void hoverElement(WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
	}

	/**
	 * random object metodu
	 * 
	 * @param contentList
	 * @return
	 */
	public Object getRandomContent(List<?> contentList) {
		Random rand = new Random();
		int n = rand.nextInt(contentList.size());
		return contentList.get(n);
	}

	public void scrollToElement(WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView({ behavior: 'smooth', block: 'end'});",
				element);
		sleep(1000);
	}

	public void switchToNextTab() {
		ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tab.get(1));
	}
}
