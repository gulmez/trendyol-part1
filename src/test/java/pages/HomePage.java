package pages;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import base.BasePage;
import static constants.HomePageConstants.*;

public class HomePage extends BasePage {

	protected final Logger log = Logger.getLogger(getClass());

	public HomePage(WebDriver driver) {
		super(driver);
	}

	/**
	 * constant eklenecek
	 * 
	 * @return
	 */

	public HomePage checkMainPageTabs() {
		/**
		 * tüm tabların likleri gezilirken ,her sayfadaki component altındaki img tagi
		 * yoksa log bascak
		 */

		WebElement el = null;
		for (int i = 0; i < getElementsBy(TAB_LINK).size(); i++) {
			clickByWebElement(getElementsBy(TAB_LINK).get(i).findElement(TAG_A));
			el = getElementsBy(TAB_LINK).get(i).findElement(TAG_A);
			log.info("--------------href=" + el.getAttribute("href") + " kontrol ediliyor--------------");
			
			getElementsBy(COMPONENT_ITEM).stream().forEach(component -> {
				if (!isElementPresent(component, TAG_IMG)) {
					log.error("'" + component.findElement(TAG_A).getAttribute("src")
							+ "' isimli component img tagi bulunamadı!");
				}
				log.info("'" + component.findElement(TAG_IMG).getAttribute("src") + "' isimli component img mevcut!");
			});
			log.info("--------------href=" + el.getAttribute("href") + " kontrol edildi--------------");
		}
		return new HomePage(driver);
	}

	public HomePage clickAndCheckRandomComponent() {
		WebElement randomButikElement = (WebElement) getRandomContent(getElementsBy(COMPONENT_ITEM_A));
		clickByWebElement(randomButikElement);
		/**
		 * Bazı ürünler 'p-card-wrppr' bazıları 'boutique-product'
		 */
		if (!getElementsBy(CARD_A).isEmpty()) {
			getElementsBy(CARD_A).stream().forEach(product -> {
				if (!isElementPresent(product, CARD_IMG)) {
					log.error("'" + product.findElement(TAG_A).getAttribute("href")
							+ "' isimli product img tagi bulunamadı!");
				}
				log.info("'" + product.findElement(CARD_IMG).getAttribute("alt") + "' isimli product img mevcut!");
			});
		} else {
			getElementsBy(BOUTIQUE_PRODUCT_A).stream().forEach(product -> {
				if (!isElementPresent(product, CARD_IMG)) {
					log.error("'" + product.findElement(TAG_A).getAttribute("href")
							+ "' isimli product img tagi bulunamadı!");
				}
				log.info("'" + product.findElement(CARD_IMG).getAttribute("src") + "' isimli product img mevcut!");
			});
		}

		return new HomePage(driver);
	}

	public HomePage checkAndAddToBasket() {
		WebElement randomProductElement = null;
		boolean isNewWindowsTab = false;
		if (isElementPresent(BOUTIQUE_PRODUCT_A)) {
			randomProductElement = (WebElement) getRandomContent(getElementsBy(BOUTIQUE_PRODUCT_A));
		} else if (isElementPresent(CARD_A)) {
			randomProductElement = (WebElement) getRandomContent(getElementsBy(CARD_A));
			isNewWindowsTab = true;
		}
		clickByWebElement(randomProductElement);
		if (isNewWindowsTab) {
			switchToNextTab();
		}
		int beforeCount = Integer.parseInt(elementGetText(BASKET_ITEM_COUNT));
		System.out.println("beforeCount:" + beforeCount);
		if (isElementPresent(ADD_TO_BS)) {
			clickObjectBy(ADD_TO_BS);
			Assert.assertTrue("Sepete Ürün Eklenemedi!",
					Integer.parseInt(elementGetText(BASKET_ITEM_COUNT)) == (beforeCount + 1));
		} else {
			log.info("'" + elementGetText(BASKET_ITEM_COUNT) + "' Ürününde Sepete Ekle butonu bulunamadı!");
		}
		return new HomePage(driver);
	}

}
