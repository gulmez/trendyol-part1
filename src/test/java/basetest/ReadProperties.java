package basetest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ReadProperties {
	static Properties envProperties;
	public static final Logger logger = Logger.getLogger(ReadProperties.class);

	static {
		envProperties = new Properties();
		try {
			InputStream propertiesStream = BaseTest.class.getClassLoader()
					.getResourceAsStream("conf/" + BaseTest.environment + ".properties");
			envProperties.load(propertiesStream);
			propertiesStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getProperty(String property) {
		String findPro = envProperties.getProperty(property);
		logger.info(property + " at properties file: " + findPro);
		return findPro;
	}
}
