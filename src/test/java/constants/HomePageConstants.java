package constants;

import org.openqa.selenium.By;

public class HomePageConstants {
	public static final By TAB_LINK = By.cssSelector(".tab-link");
	public static final By COMPONENT_ITEM = By.className("component-item");
	public static final By TAG_IMG = By.tagName("img");
	public static final By TAG_A = By.tagName("a");
	public static final By COMPONENT_ITEM_A = By.cssSelector(".component-item a");
	public static final By CARD_A = By.cssSelector(".p-card-wrppr a");
	public static final By CARD_IMG = By.className("p-card-img");
	public static final By BOUTIQUE_PRODUCT_A = By.cssSelector(".boutique-product a");
	public static final By ADD_TO_BS = By.className("add-to-bs");
	public static final String BASKET_ITEM_COUNT = ".basket-item-count-container";
	}
