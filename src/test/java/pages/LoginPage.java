package pages;

import static constants.LoginPageConstants.*;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import base.BasePage;

public class LoginPage extends BasePage {
	protected final Logger log = Logger.getLogger(getClass());

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	public HomePage login(String email, String password) {
		closedPopup(FANCYBOX_CLOSE);
		hoverElement(getElementBy(ACCOUNT_USER));
		clickObjectBy(LOGIN_BUTTON);
		boolean validateEmail = isElementPresent(EMAIL_INPUT);
		boolean validatePassword = isElementPresent(PASSWORD_INPUT);
		boolean validateLoginButton = isElementPresent(SUBMIT);
		Assert.assertTrue("Email ile giriş sayfa yapısında bozulma var.",
				validateEmail && validatePassword && validateLoginButton);
		sendKeysBy(EMAIL_INPUT, email);
		sendKeysBy(PASSWORD_INPUT, password);
		clickObjectBy(SUBMIT);
		boolean validateLogin = isElementPresent(By.cssSelector(USER_NAME));
		Assert.assertTrue("Username alanı bulunamadı", validateLogin);
		closedPopup(MODAL_CLOSE);
		hoverElement(getElementBy(I_USER));
		log.info("user-name:" + elementGetText(USER_NAME));
		Assert.assertFalse("Giriş yapılamadı!", elementGetText(USER_NAME).isEmpty());
		log.info("Giriş yapılamadı!");
		return new HomePage(driver);
	}

	public void closedPopup(By close) {
		if (isElementPresent(close)) {
			clickObjectBy(close);
		}
	}
}
